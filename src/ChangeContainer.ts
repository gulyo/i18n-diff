import { ChangeResult } from "./ChangeResult";
import { ChangeStatus } from "./ChangeStatus";

export interface ChangeContainer {
  files: { [key: string]: ChangeStatus.DELETED };
  entries: { [key: string]: ChangeResult };
}
