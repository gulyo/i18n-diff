import { FileContainer } from "./FileContainer";
import { logger } from "./logger";
import { config } from "./config";
import { Dictionary } from "./Dictionary";
import fs from "fs";

export class FileListIterator implements Iterator<FileContainer> {
  private index = 0;

  constructor(private files: string[] = []) {}

  private static READ_FILE(path: string): Dictionary | undefined {
    if (!fs.existsSync(path)) {
      return undefined;
    }
    const content = fs.readFileSync(path, { encoding: "utf-8" });
    return JSON.parse(content) as Dictionary;
  }

  // Return the current element.
  public current(): FileContainer {
    logger.debug(this.files[this.index], config.fromDir, config.toDir);
    return {
      fileName: this.files[this.index],
      from: FileListIterator.READ_FILE(
        `${config.fromDir}/${this.files[this.index]}`
      ) as Dictionary,
      to: FileListIterator.READ_FILE(
        `${config.toDir}/${this.files[this.index]}`
      ),
    };
  }

  // Return the current element and move forward to next element.
  public next(): IteratorResult<FileContainer> {
    if (!this.valid()) {
      return { done: true, value: undefined };
    }
    const current = this.current();
    ++this.index;
    return {
      done: false,
      value: current,
    };
  }

  // Return the key of the current element.
  public key(): number {
    return this.index;
  }

  // Checks if current position is valid.
  public valid(): boolean {
    return this.index > -1 && this.index < this.files.length;
  }

  // Rewind the Iterator to the first element.
  public rewind(): void {
    this.index = 0;
  }
}
