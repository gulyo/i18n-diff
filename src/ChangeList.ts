import { FileList } from "./FileList";
import { Dictionary } from "./Dictionary";
import { logger } from "./logger";
import { ChangeResult } from "./ChangeResult";
import { ChangeStatus } from "./ChangeStatus";
import { ChangeContainer } from "./ChangeContainer";

class ChangeList {
  private fileList: FileList = new FileList();

  public async Changes(): Promise<ChangeContainer> {
    await this.fileList.Init();
    return new Promise<ChangeContainer>((resolve) => {
      const result: ChangeContainer = {
        files: {},
        entries: {},
      };
      for (const container of this.fileList) {
        if (container.to) {
          const res = this.processProperties(container.from, container.to);
          if (!!res) {
            result.entries[container.fileName] = res;
          }
        } else {
          result.files[container.fileName] = ChangeStatus.DELETED;
        }
      }
      resolve(result);
    });
  }

  private processProperties = (
    from: Dictionary,
    to: Dictionary
  ): ChangeResult | null => {
    const result: ChangeResult = {};
    for (const key in from) {
      switch (typeof from[key]) {
        case "string":
          if (!to[key]) {
            result[key] = ChangeStatus.REMOVED;
            break;
          }
          if (from[key] !== to[key]) {
            result[key] = ChangeStatus.CHANGED;
            break;
          }
          break;
        case "object":
          if (!to[key]) {
            result[key] = ChangeStatus.REMOVED;
            break;
          }
          const objRes = this.processProperties(
            from[key] as Dictionary,
            to[key] as Dictionary
          );
          if (!!objRes) {
            result[key] = objRes;
          }
          break;
        default:
          logger.error("Unexpected type in Dictionary", key);
          break;
      }
    }
    for (const key in to) {
      if (!to[key]) {
        result[key] = ChangeStatus.NEW;
        break;
      }
    }
    if (!Object.keys(result).length) {
      return null;
    }
    return result;
  };
}

export const changeList: ChangeList = new ChangeList();
