export enum ChangeStatus {
	CHANGED = "changed",
	REMOVED = "removed",
	DELETED = "deleted",
	NEW = "new"
}
