import { ChangeStatus } from "./ChangeStatus";

export interface ChangeResult {
	[key: string]: ChangeStatus | ChangeResult
}