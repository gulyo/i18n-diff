import express, { Express } from "express";
import { git } from "./GitHandler";
import { logger } from "./logger";
import { changeList } from "./ChangeList";
import { changeCsv } from "./ChangeCsv";

export class Server {
  private app: Express;

  constructor() {
    this.app = express();
    this.init();
  }

  public Start() {
    this.app.listen(8080, () => {
      logger.info("Server started");
    });
  }

  private init() {
    // <editor-fold desc="GitHandler">
    this.initRoot();
    this.initClone();
    this.initStatus();
    this.initLog();
    this.initFetch();
    this.initPull();
    this.initDiff();
    this.initCheckout();
    // </editor-fold desc="GitHandler">

    // <editor-fold desc="ChangeList">
    this.initChange();
    this.initCsv();
    // </editor-fold desc="ChangeList">
  }

  private initRoot() {
    this.app.get("/", (_req, res) => {
      res.send({
        "/clone": `Clone the two repositories with default branch`,
        "/status": `Status of the 'to' (latest) repo`,
        "/log": `Show the log of master branch (releases)`,
        "/pull": `Get the latest version of 'to' branch (get up to date)`,
        "/fetch": `Refresh the indexes of both repos`,
        "/diff": `Unformatted difference between the 'from' repo commit and origin/UAT`,
        "/checkout/<hash>": `Checkout the required version to 'from' repo`,
        "/change": `List the translation changes between 'from' and 'to'`,
        "/csv": `Fetch the csv form of '/change'`,
      });
    });
  }

  private initClone() {
    this.app.get("/clone", async (_req, res) => {
      try {
        await git.Clone();
        res.json({
          status: await git.Status(),
        });
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initStatus() {
    this.app.get("/status", async (_req, res) => {
      try {
        res.json(await git.Status());
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initLog() {
    this.app.get("/log", async (_req, res) => {
      try {
        res.json(await git.Log());
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initFetch() {
    this.app.get("/fetch", async (_req, res) => {
      try {
        res.json(await git.Fetch());
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initPull() {
    this.app.get("/pull", async (_req, res) => {
      try {
        res.json(await git.Pull());
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initDiff() {
    this.app.get("/diff", async (_req, res) => {
      try {
        res.json(await git.Diff());
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initCheckout() {
    this.app.get("/checkout/:hash", async (_req, res) => {
      try {
        res.send(await git.Checkout(_req.params.hash));
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initChange() {
    this.app.get("/change", async (_req, res) => {
      try {
        res.json(await changeList.Changes());
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }

  private initCsv() {
    this.app.get("/csv", async (_req, res) => {
      try {
        res.header("Content-Type", "text/csv");
        res.attachment("WeblateChanges.csv");
        res.send(await changeCsv.Csv());
      } catch (e) {
        logger.error(e);
        res.send(e);
      }
    });
  }
}
