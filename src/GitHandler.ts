import simpleGit, {
  DiffResult,
  FetchResult,
  LogResult,
  PullResult,
  SimpleGit,
  SimpleGitOptions,
  StatusResult,
  TaskOptions,
} from "simple-git";
import fs from "fs";
// @ts-ignore
import fsUtils from "nodejs-fs-utils";
import { config } from "./config";
import { logger } from "./logger";

class GitHandler {
  private static readonly OPTIONS_TO: SimpleGitOptions = {
    baseDir: config.toDir,
    binary: "git",
    maxConcurrentProcesses: 6,
    config: [],
  };
  private static readonly OPTIONS_FROM: SimpleGitOptions = {
    baseDir: config.fromDir,
    binary: "git",
    maxConcurrentProcesses: 6,
    config: [],
  };
  protected gitTo: SimpleGit;
  protected gitFrom: SimpleGit;

  constructor() {
    [config.workDir, config.fromDir, config.toDir].forEach((dir: string) => {
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
    });

    this.gitFrom = simpleGit(GitHandler.OPTIONS_FROM);
    this.gitTo = simpleGit(GitHandler.OPTIONS_TO);
  }

  public async Clone() {
    if (fs.existsSync(config.workDir)) {
      fsUtils.rmdirsSync(config.workDir);
    }
    fs.mkdirSync(config.workDir);
    fs.mkdirSync(config.toDir);
    fs.mkdirSync(config.fromDir);
    await this.gitFrom.clone(config.repo, config.fromDir);
    // await this.gitFrom.cwd({path: config.fromDir, root: false});
    // await this.gitFrom.checkout(GitHandler.COMMIT_FROM);

    await this.gitTo.clone(config.repo, config.toDir);
    await this.gitTo.cwd({ path: config.toDir, root: false });
  }

  public async Status() {
    return new Promise<StatusResult>((resolve) => {
      this.gitTo.status().then((result) => resolve(result));
    });
  }

  public async Log() {
    return new Promise<LogResult>((resolve) => {
      this.gitTo
        .log(["--first-parent", "origin/master"])
        .then((result) => resolve(result));
    });
  }

  public async Fetch() {
    return new Promise<{ from: FetchResult; to: FetchResult }>((resolve) => {
      this.gitTo
        .fetch()
        .then((resTo) =>
          this.gitFrom
            .fetch()
            .then((resFrom) => resolve({ to: resTo, from: resFrom }))
        );
    });
  }

  public async Pull() {
    return new Promise<PullResult>((resolve) => {
      this.gitTo.pull().then((result) => resolve(result));
    });
  }

  public async Diff() {
    return new Promise<DiffResult>(async (resolve) => {
      logger.debug(
        `origin/${await this.gitFrom.revparse(["--verify", "HEAD"])}`,
        `origin/${await this.gitTo.revparse(["--verify", "HEAD"])}`
      );
      this.gitTo
        .diffSummary([
          await this.gitFrom.revparse(["--verify", "HEAD"]),
          await this.gitTo.revparse(["--verify", "HEAD"]),
        ] as TaskOptions)
        .then((summary) => resolve(summary));
    });
  }

  public async Checkout(hash: string) {
    return new Promise<string>(async (resolve) => {
      this.gitFrom
        .checkout([hash] as TaskOptions)
        .then((summary) => resolve(summary));
    });
  }
}

export const git: GitHandler = new GitHandler();
