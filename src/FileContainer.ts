import { Dictionary } from "./Dictionary";

export interface FileContainer {
  fileName: string;
  from: Dictionary;
  to?: Dictionary;
}
