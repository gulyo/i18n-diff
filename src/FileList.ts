import { FileContainer } from "./FileContainer";
import { FileListIterator } from "./FileListIterator";
import { git } from "./GitHandler";

export class FileList implements Iterable<FileContainer> {

	private files: string[] = [];

	public get List(): string[] {
		return this.files;
	}

	public async Init(): Promise<void> {
		this.files = await this.listFiles();
	}

	public [Symbol.iterator](): Iterator<FileContainer> {
		return new FileListIterator(this.List);
	}

	private async listFiles(): Promise<string[]> {
		return new Promise(async resolve => {
			const changeList = (await git.Diff()).files.filter(
				file => file.file.match(/\.en\.json$/)
			).map(file => file.file);
			resolve(changeList);
		});


	}

}

