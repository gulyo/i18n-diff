import { changeList } from "./ChangeList";
import { ChangeContainer } from "./ChangeContainer";
import { ChangeResult } from "./ChangeResult";

class ChangeCsv {
  private static get header(): string {
    return `FileChange;File;Key;Change\n`;
  }

  private static files(change: ChangeContainer): string {
    return Object.entries(change.files).reduce(
      (res, cur) => `${res}${cur[1]};${cur[0]};;\n`,
      ``
    );
  }

  public async Csv(): Promise<string> {
    return new Promise(async (resolve) => {
      let result = ChangeCsv.header;
      const change: ChangeContainer = await changeList.Changes();
      result = `${result}${ChangeCsv.files(change)}`;
      result = `${result}${this.entries(change)}`;
      resolve(result);
    });
  }

  private entries(change: ChangeContainer): string {
    // Root level is always a file name
    return Object.entries(change.entries).reduce(
      (res, [key, value]) =>
        `${res}${this.processEntry(``, value)
          .map((line) => `;${key};${line}`)
          .join("")}`,
      ``
    );
  }

  private processEntry(prefix: string, entry: ChangeResult): string[] {
    let res: string[] = [];
    Object.entries(entry).forEach(([key, value]) => {
      if (typeof value === "string") {
        res.push(`${prefix}${prefix ? "." : ""}${key};${value}\n`);
      } else {
        res = [
          ...res,
          ...this.processEntry(`${prefix}${prefix ? "." : ""}${key}`, value),
        ];
      }
    });
    return res;
  }
}

export const changeCsv = new ChangeCsv();
