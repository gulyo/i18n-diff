import os from "os";

export const config = {
  repo: `ssh://git@stash-ext.hilti.com:7999/ddu/hdms-frontend.git`,

  workDir: `${os.homedir()}/tmp/hdms-frontend/`,
  fromLocation: `from`,
  toLocation: `to`,

  get fromDir() {
    return `${this.workDir}${this.fromLocation}`;
  },
  get toDir() {
    return `${this.workDir}${this.toLocation}`;
  },
};
